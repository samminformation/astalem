﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Astalem.Data;
using Astalem.Models.AstalemModel;

namespace Astalem.Controllers
{
    public class ProposalsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProposalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Proposals
        public async Task<IActionResult> Index()
        {
            return View(await _context.Proposals.ToListAsync());
        }

        // GET: Proposals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposals = await _context.Proposals
                .SingleOrDefaultAsync(m => m.Id == id);
            if (proposals == null)
            {
                return NotFound();
            }

            return View(proposals);
        }

        // GET: Proposals/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Proposals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProposalerName,ProposalerPhone,ProposalerTitle,ProposalerContain")] Proposals proposals)
        {
            if (ModelState.IsValid)
            {
                _context.Add(proposals);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(proposals);
        }

        // GET: Proposals/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposals = await _context.Proposals.SingleOrDefaultAsync(m => m.Id == id);
            if (proposals == null)
            {
                return NotFound();
            }
            return View(proposals);
        }

        // POST: Proposals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProposalerName,ProposalerPhone,ProposalerTitle,ProposalerContain")] Proposals proposals)
        {
            if (id != proposals.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(proposals);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProposalsExists(proposals.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(proposals);
        }

        // GET: Proposals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposals = await _context.Proposals
                .SingleOrDefaultAsync(m => m.Id == id);
            if (proposals == null)
            {
                return NotFound();
            }

            return View(proposals);
        }

        // POST: Proposals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var proposals = await _context.Proposals.SingleOrDefaultAsync(m => m.Id == id);
            _context.Proposals.Remove(proposals);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProposalsExists(int id)
        {
            return _context.Proposals.Any(e => e.Id == id);
        }
    }
}
