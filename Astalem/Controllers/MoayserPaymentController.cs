﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Astalem.Controllers
{
    public class MoayserPaymentController : Controller
    {
        private readonly ApplicationDbContext _context;
        public MoayserPaymentController(ApplicationDbContext Context)
        {
            _context = Context;
        }
        [HttpGet]
        public async Task<IActionResult> Index(int orderId)
        {
            var order = await _context.Orders.FindAsync(orderId);
            if (order != null || order.Amount<5)
            {

                if (order.Amount >= 300)
                {
                    return Ok("لا يمكنك استخدام الدفع الالكتروني في حال الطلب يذيد عن 300 ريال ");
                }
                else
                {
                    PaymentInput paymentInput = new PaymentInput();
                    paymentInput.callback_url = "https://astalem.com/MoayserPayment/PaymentCallBack?orderId=" + orderId + "&amount=" + order.Amount;
                    paymentInput.publishable_api_key = "pk_live_99TsKrkmQnjxNdDEnqBuTGs75vJSS9U6UxxkEsWi";
                    paymentInput.paymentType = "creditcard";
                    paymentInput.amount = order.Amount;
                    paymentInput.regId = "orderId is :" + orderId.ToString();

                    paymentInput.amountSAR = Convert.ToInt32(order.Amount * 100) ;
                    return View(paymentInput);
                }
            }
            else
            {
                return Ok("Error");
            }
        }

        //                paymentInput.publishable_api_key = "sk_test_1PGzrsYDKf9nyXAQ86nroFJE8u1KMm8xtCSHzKZx";

        [HttpGet]
        public async Task<IActionResult> PaymentCallBack(int orderId, double amount, string id, string status, string message)
        {
            if(status == "paid")
            {
                var order =await _context.Orders.FindAsync(orderId);
                 
                 order.AmountPayed = order.Amount;
                _context.Update(order);
                await _context.SaveChangesAsync();

                try {
                    var provider = await _context.Providers.SingleOrDefaultAsync(m => m.Phone == order.ProviderPhone);
                    await Firebase.SendMessage(provider.Token, "استلم",  $"قام العميل بدفع مبلغ {amount} للطلب رقم " + order.Id, ""+order.Id, "order");
                  await  AddBlanceOrderToProvider(order.ProviderPhone,order.AmountWithoutTax);
                }
                catch (Exception e) { }
          

                return RedirectToAction("PaymentDone");
            }
            else
            {
                ViewData["message"] = message;   
            return Ok(message+status);
            }
        }




        [HttpGet]
        public async Task<IActionResult> Index2(string providerPhone, double amount)
        {
            var provider = await _context.Providers.FindAsync(providerPhone);

            if (provider != null || amount < 5)
            {
                if (provider.ExpirationDate.AddDays(1) >= DateTime.Now)
                {
                    return Ok("Your Account alrady Actived");
                }
                else { 
                PaymentInput paymentInput = new PaymentInput();
                paymentInput.callback_url = "https://astalem.com/MoayserPayment/PaymentCallBack2?providerPhone=" + providerPhone;
                paymentInput.publishable_api_key= "pk_live_99TsKrkmQnjxNdDEnqBuTGs75vJSS9U6UxxkEsWi";
                    paymentInput.paymentType = "creditcard";
                paymentInput.amount = amount;
                paymentInput.amountSAR = Convert.ToInt32(amount * 100);
                    paymentInput.regId = "Provider Phone is :" + providerPhone;

                    return View(paymentInput);
            }
            }
            else
            {
                return Ok("Error");
            }
        }


        [HttpGet]
        public async Task<IActionResult> PaymentCallBack2(string providerPhone, string id, string status, string message)
        {
            if(status == "paid")
            {
                var provider = await _context.Providers.FindAsync(providerPhone);

                provider.ExpirationDate = DateTime.Now;
                _context.Update(provider);
                await _context.SaveChangesAsync();
               await AddBlanceProviderActiveHistory(providerPhone);
                //Sms To 
                return RedirectToAction("PaymentDone");
            }
            else
            {
                ViewData["message"] = message;
                return Ok(message + status);
            }
        }


        [HttpGet]
        public IActionResult PaymentDone()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> AddBlanceOrderToProvider(string providerPhone,double amount)
        {

            ProviderAccount providerAccount = new ProviderAccount();
            providerAccount.Amount = amount - 1.15;
            providerAccount.DateTimeNow = DateTime.Now;
            providerAccount.ProviderPhone = providerPhone;
            providerAccount.Type = "+";
            _context.Add(providerAccount);
            await _context.SaveChangesAsync();
            return Ok("1");
        }


        [HttpGet]
        public async Task<IActionResult> AddBlanceProviderActiveHistory(string providerPhone) 
        {
            var app = await _context.App.SingleOrDefaultAsync(m => m.Id > 0);
            ProviderActiveHistory activeHistory  = new ProviderActiveHistory();
            activeHistory.ProviderPhone = providerPhone;
            activeHistory.DateTimeNow = DateTime.Now;
            activeHistory.amount = app.AmountToActiveProvider; 
            _context.Add(activeHistory);
            await _context.SaveChangesAsync();
            return Ok("1");
        }


        public class PaymentInput
        {
            [Required]
            public string publishable_api_key { get; set; }
            [Required]
            public string callback_url { get; set; }
            [Required]
            public double amount { get; set; }
            [Required]
            public int amountSAR { get; set; }
            [Required]
            public string paymentType { get; set; }
            public string regId { get; set; }
        }

        public class Errors
        {
            public List<string> company { get; set; }
            public List<string> last_name { get; set; }
            public List<string> month { get; set; }
            public List<string> year { get; set; }
            public List<string> name { get; set; }
        }

        public class MoayserErroModel
        {
            public string type { get; set; }
            public string message { get; set; }
            public Errors errors { get; set; }
        }

    }
}
