﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Astalem.Models;
using Microsoft.AspNetCore.Authorization;
using Astalem.Data;
using Astalem.Models.AstalemViewModels;
using Microsoft.EntityFrameworkCore;
using jsreport.AspNetCore;
using jsreport.Types;
using System.Web;

namespace Astalem.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IJsReportMVCService _JsReportMvcService { get; }

        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext Context, IJsReportMVCService JsReportMVCService)
        {
            _context = Context;
            _JsReportMvcService = JsReportMVCService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            IndexViewModel indexViewModel = new IndexViewModel();

           List<City> citys = new List<City>();
            var prv = await _context.Providers.ToListAsync();
            var allAreaAndCity = await _context.Districts.ToListAsync();
            foreach (var item in allAreaAndCity)
            {
                City city = new City();
                city.AreaName = item.DistrictName;
                city.CityName = item.City;
                var provider = prv.Where(m => m.DistrictId == item.Id).ToList();  
                city.CountProviderInArea = provider.Count;
                citys.Add(city);
            }

          
            var clients = await _context.Clients.ToListAsync();
            var providers = await _context.Providers.ToListAsync(); 
            var orders = await _context.Orders.ToListAsync(); 
            var todayOrders = await _context.Orders.Where(m=>m.DateTimeNow.Date == DateTime.Now.Date).ToListAsync(); 
            var todayAmount = await _context.ProviderActiveHistories.Where(m=>m.DateTimeNow.Date == DateTime.Now.Date ).ToListAsync(); 
            var todayAllAmount = await _context.ProviderActiveHistories.ToListAsync(); 
            indexViewModel.ClientCount = clients.Count;
            indexViewModel.ProviderCount= providers.Count;
            indexViewModel.AllOrderCount = orders.Count;
            indexViewModel.TodayOrderCount = todayOrders.Count;
            indexViewModel.TodayAmount = todayAmount.Sum(m=>m.amount);
            indexViewModel.AllAmount = todayAllAmount.Sum(m=>m.amount);
            indexViewModel.Cities = citys.OrderByDescending(m => m.CountProviderInArea).ToList();
            return View(indexViewModel);
        }

        [HttpGet]
        public IActionResult ReportIndex()  
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }


        [HttpGet]
        public IActionResult OrderReportIndex() 
        {
            ReportTimeViewModel reportTimeViewModel = new ReportTimeViewModel();
            reportTimeViewModel.DateFrom = DateTime.Now.Date;
            reportTimeViewModel.DateTo = DateTime.Now.Date;
            return View(reportTimeViewModel);
        }


        [HttpGet]
      //  [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> ProviderReport()
        {


            //string header = "";
            //string footer = "";
            //header = await _JsReportMvcService.RenderViewToStringAsync(HttpContext, RouteData, "header", new { });
            //footer = await _JsReportMvcService.RenderViewToStringAsync(HttpContext, RouteData, "footer", new { });


            var providers =await  _context.Providers.Include(m=>m.District).ToListAsync();
            List<ProviderReportViewModel> providerReportViewModel = new List<ProviderReportViewModel>();
            foreach (var item in providers) 
            {
                ProviderReportViewModel reportViewModel = new ProviderReportViewModel();
                var allAmount=  await _context.ProviderActiveHistories.Where(m => m.ProviderPhone == item.Phone).ToListAsync();
                var ProviderAmount =  await _context.ProviderAccounts.Where(m => m.ProviderPhone == item.Phone).ToListAsync();    
                var count = await _context.Orders.Where(m => m.ProviderPhone == item.Phone).ToListAsync();
                var countToday =  count.Where(m =>  m.DateTimeNow.Date == DateTime.Now.Date).ToList();


                reportViewModel.AccountName = item.BankAccountName;
                reportViewModel.AccountNumber = item.IPanNumber;
                reportViewModel.BankName = item.BankName;
                reportViewModel.AllAmount = allAmount.Sum(s => s.amount);
                reportViewModel.Area = item.District.DistrictName;
                reportViewModel.City = item.District.City;
                reportViewModel.OrderCount =count.Count ;
                reportViewModel.TodayOrderCount =countToday.Count ;
                reportViewModel.ProviderPhone = item.Phone;
                reportViewModel.ProviderName = item.FullName;
                reportViewModel.ProviderAmount = ProviderAmount.Sum(m=>m.Amount);
                providerReportViewModel.Add(reportViewModel);
            }


            //string mss = DateTime.Now.ToString();
            //HttpContext.JsReportFeature()
            //    .Recipe(Recipe.PhantomPdf).OnAfterRender(r => HttpContext.Response.Headers["Content-Disposition"] = @"attachment; filename=" + HttpUtility.UrlEncode(mss) + ".pdf")
            //  .Configure(r => r.Template.Phantom = new Phantom
            //  {
            //      Header = header,
            //      Footer = footer,
            //      HeaderHeight = "3.0cm",
            //      FooterHeight = "3.2cm",
            //      Margin = "{ \"top\": \"-10px\", \"left\": \"0px\", \"right\": \"0px\", \"bottom\": \"-20px\" }",
            //      Format = PhantomFormat.A4
            //  });

            return View(providerReportViewModel.ToList());
        }

        [HttpGet]
        public async Task<IActionResult> OrderReport(DateTime DateTo, DateTime DateFrom) 
        {
            ViewData["Message"] = DateTo;   

            var order =await _context.Orders.Where(m=>m.DateTimeNow.Date >= DateFrom.Date && m.DateTimeNow.Date <= DateTo.Date).ToListAsync();
            OrderReportViewModel orderReport = new OrderReportViewModel();

            orderReport.From =DateFrom.Date;
            orderReport.To = DateTo.Date;
            orderReport.OrderCount = order.Count;
            orderReport.AllAmount = order.Sum(m=>m.Amount);
            orderReport.OrderCashCount = order.Where(m=>m.PayType == "Cash").Count();
            orderReport.OrderOnLineCount = order.Where(m => m.PayType == "OnLine"|| m.PayType == "Online").Count();
            orderReport.OrderInRoadCount = order.Where(m => m.Statues == "4").Count();
            orderReport.OrderInAttendCount = order.Where(m => m.Statues == "5").Count();
            orderReport.OrderFinshedCount = order.Where(m => m.Statues == "1").Count();
            orderReport.OrderCanceledCount = order.Where(m => m.Statues == "2" || m.Statues == "3").Count();

            return View(orderReport);
        }




        [HttpGet]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
        [HttpGet]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        [HttpGet]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
