﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Astalem.Data;
using Astalem.Models.AstalemModel;

namespace Astalem.Controllers
{
    public class ProviderAccountsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProviderAccountsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ProviderAccounts
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProviderAccounts.Include(p => p.Provider);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ProviderAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var providerAccount = await _context.ProviderAccounts
                .Include(p => p.Provider)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (providerAccount == null)
            {
                return NotFound();
            }

            return View(providerAccount);
        }

        // GET: ProviderAccounts/Create
        public IActionResult Create(string phone)
        {
           
            ViewData["allAmount"] = _context.ProviderAccounts.Where(m => m.ProviderPhone == phone).Sum(m => m.Amount);
            ViewData["ProviderPhone"] = phone;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ActiveProvider(string phone)
        {
            var provider = await _context.Providers.FindAsync(phone);
            provider.ExpirationDate = DateTime.Now;
            _context.Update(provider);
            await _context.SaveChangesAsync();
            return RedirectToAction("ProviderReport", "Home");
        }
        // POST: ProviderAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Amount,DateTimeNow,Type,ProviderPhone")] ProviderAccount providerAccount)
        {
            if (ModelState.IsValid)
            {
                providerAccount.Amount = providerAccount.Amount * -1;
                providerAccount.DateTimeNow = DateTime.Now;
                _context.Add(providerAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction("ProviderReport", "Home");
            }
        //    ViewData["ProviderPhone"] = new SelectList(_context.Providers, "Phone", "Phone", providerAccount.ProviderPhone);
            return View(providerAccount);
        }

        // GET: ProviderAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var providerAccount = await _context.ProviderAccounts.SingleOrDefaultAsync(m => m.Id == id);
            if (providerAccount == null)
            {
                return NotFound();
            }
            ViewData["ProviderPhone"] = new SelectList(_context.Providers, "Phone", "Phone", providerAccount.ProviderPhone);
            return View(providerAccount);
        }

        // POST: ProviderAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Amount,DateTimeNow,Type,ProviderPhone")] ProviderAccount providerAccount)
        {
            if (id != providerAccount.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(providerAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProviderAccountExists(providerAccount.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProviderPhone"] = new SelectList(_context.Providers, "Phone", "Phone", providerAccount.ProviderPhone);
            return View(providerAccount);
        }

        // GET: ProviderAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var providerAccount = await _context.ProviderAccounts
                .Include(p => p.Provider)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (providerAccount == null)
            {
                return NotFound();
            }

            return View(providerAccount);
        }

        // POST: ProviderAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var providerAccount = await _context.ProviderAccounts.SingleOrDefaultAsync(m => m.Id == id);
            _context.ProviderAccounts.Remove(providerAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProviderAccountExists(int id)
        {
            return _context.ProviderAccounts.Any(e => e.Id == id);
        }
    }
}
