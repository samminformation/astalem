﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astalem;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Astalem.Models.AstalemViewModels;
using Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace Astalem.Controllers.Api   
{
    [Produces("application/json")]
    [Route("api/Order/[Action]")]

    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHubContext<MainHub> _hubcontext { get; set; }
        public OrderController(ApplicationDbContext Context, IHubContext<MainHub> Hubcontext)
        {
            _context = Context;
            _hubcontext = Hubcontext;
        }
    
        [HttpPost]
        public async Task<IActionResult> AddOrder([FromBody] Order order)
        {
            ResponseBody responseBody = new ResponseBody();
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    transaction.Commit();
                    order.DateTimeNow = DateTime.Now;
                    order.ProviderPhone = "0";
                    order.Amount = 0;
                    order.AmountPayed = 0;
                    order.AmountWithoutTax = 0;
                    var near =await GetNearProvider(order.OrderLat, order.OrderLong);    


                    var getnearandnotpusy = ProviderBusy(near);//select list from near provider and active and not Busy 

                    _context.Add(order);//add order in order state 
                    await _context.SaveChangesAsync();
                  //  {
                        await AddOrderInOrderState(order.ClientPhone, order.Id, near);//add order in state
                        await SendOrderToProvider(order.Id, near);// sent to providers
                        responseBody.Message = "1";
                        return Ok(responseBody);
                  //  }
                 //   else
                 //   {
                     //   responseBody.Message = "Model Error";
                   //     return Ok(responseBody);
                  //  }
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    responseBody.Error = "-1";
                    return Ok(responseBody);
                }
            }
        }
        [HttpGet]
        public double GetDistance(double currentLat, double currentLong, double otherLat, double otherLong)
        {

            try
            {
                double d = Math.Acos(
                             Math.Sin(Math.PI * currentLat / 180.0) *
                             Math.Sin(Math.PI * otherLat / 180.0) +
                             Math.Cos(Math.PI * currentLat / 180.0) *
                             Math.Cos(Math.PI * otherLat / 180.0) *
                             Math.Cos(Math.PI * otherLong / 180.0 -
                                      Math.PI * currentLong / 180.0)) * 6371;
                if (d < 0)
                { return 0; }
                else { return d; }
            }
            catch
            {
                return 0;
            }
        }
        [HttpGet]
        public async Task< List<Provider>> GetNearProvider(double clientLat, double clientLong)//GetNearProvider and active and open statuse
        {
            try { 
            var allProvider =await _context.Providers.Where(m => m.Connectivity == "1"  && m.ConnectionId.Length > 5 && m.ExpirationDate.AddDays(1) >= DateTime.Now).ToListAsync();
            List<Provider> providers = new List<Provider>();
            foreach (var item in allProvider)
            {
                double providerDistance = GetDistance(clientLat, clientLong, item.Lat, item.Log);
                if (providerDistance < 50) { providers.Add(item); }
            }
            return providers;
            } 
            catch (Exception x)
            {
                return null;
            }
        }
        [HttpGet]
        public async Task<IActionResult> AddOrderInOrderState(string clientPhone, int orderId, List<Provider> providers)
        {
            try { 
            foreach (var item in providers)
            {
                var id = _context.OrderStates.SingleOrDefault(m => m.ProviderPhone == item.Phone);
                if (id == null)
                {
                    OrderState orderStatues = new OrderState();
                    orderStatues.ProviderPhone = item.Phone;
                    orderStatues.OrderId = orderId;
                    orderStatues.ClientPhone = clientPhone;
                    orderStatues.Stateus = 0;
                    _context.Add(orderStatues);
                    await _context.SaveChangesAsync();
                }


            }
            return Ok(1);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<List<Provider>> SendOrderToProvider(int orderId, List<Provider> providers)
        {
            try { 
            if (providers != null)
            {
                foreach (var item in providers)
                {
                    await _hubcontext.Clients.Client(item.ConnectionId).SendAsync("ReceiveOrder", orderId);
                    await Firebase.SendMessage(item.Token, "استلم", " هنالك طلب جديد",orderId.ToString(), "order2");

                    }
                }

            return providers;
            }
            catch (Exception x)
            {
                return null;
            }
        }
        [HttpGet]
        public List<Provider> ProviderBusy(List<Provider> providers)
        {

            try { 
            List<Provider> providerNotBusy = new List<Provider>();
            if (providers.Count>0)
            {
                foreach (var item in providers)
                {                        
                        var busy = _context.OrderStates.SingleOrDefault(model => model.ProviderPhone == item.Phone);
                    if (busy == null)
                        providerNotBusy.Add(item);  
                }
            }
            
                
                
            return providerNotBusy;
            }
            catch (Exception x)
            {
                return null;
            }
        }
        [HttpGet]
        public async Task<IActionResult> TestSentOrder(string providerPhone, int orderId)
        {
            try
            {
                var item = await _context.Providers.FindAsync(providerPhone);
                await _hubcontext.Clients.Client(item.ConnectionId).SendAsync("ReceiveOrder", orderId);
                return Ok("1");
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public double GetCoboneAmount(string cobonCode)
        {
            var cobon = _context.Cobones.SingleOrDefault(c => c.CoboneCode == cobonCode && c.ExpirationDate.Date <= DateTime.Now.Date);
            if (cobon == null)
                return 0;
            else
                return 0 + cobon.CoboneValue;
        }
        [HttpGet]
        public async Task<IActionResult> ProviderGetOrderDetails(int orderId)
        {
            try { 
            ProviderRecieveOrderDetilesViewModel providerRecieveOrderDetilesViewModel = new ProviderRecieveOrderDetilesViewModel();
            var order =await _context.Orders.Include(m=>m.Client).SingleOrDefaultAsync(m=>m.Id==orderId);
            if(order != null)
            {
                providerRecieveOrderDetilesViewModel.OrderId = order.Id;
                providerRecieveOrderDetilesViewModel.DescriptionLocation = order.LocationDescription;
                providerRecieveOrderDetilesViewModel.DescriptionOrder = order.OrderDescription;
                providerRecieveOrderDetilesViewModel.ClientPhone = order.Client.Phone;
                providerRecieveOrderDetilesViewModel.ClientImage = order.Client.Image;
                providerRecieveOrderDetilesViewModel.UserLat = order.UserLat;
                providerRecieveOrderDetilesViewModel.UserLong = order.UserLong;
                providerRecieveOrderDetilesViewModel.OrderLat = order.OrderLat;
                providerRecieveOrderDetilesViewModel.OrderLong = order.OrderLong;
                providerRecieveOrderDetilesViewModel.DistanceOrderLocationFromClientLocation = GetDistance(order.UserLat,order.UserLong,order.OrderLat,order.OrderLong);
            }
          
            return Ok(providerRecieveOrderDetilesViewModel);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> ProviderRejectOrder(string providerPhone)
        {
            try
            {

            
            ResponseBody responseBody = new ResponseBody();
            var orderState = await _context.OrderStates.Where(m => m.ProviderPhone == providerPhone).ToListAsync();
            if (orderState != null)
            {
                _context.RemoveRange(orderState);
                await _context.SaveChangesAsync();
            }
            responseBody.Message = "1";
            return Ok(responseBody);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> ChangOrderToInRoad(int orderId)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var order = await _context.Orders.Include(m => m.Client).SingleOrDefaultAsync(m => m.Id == orderId && m.Statues == "5");
            if (order != null)
            {
                order.Statues = "4";
                _context.Update(order);
                await _context.SaveChangesAsync();
                await Firebase.SendMessage(order.Client.Token, "استلم", "طلبك برقم" + order.Id + "بالطريق ", "" + order.Id, "order");
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Moder Erorr";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> ChangOrderToAttend(int orderId)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var order = await _context.Orders.Include(m => m.Client).SingleOrDefaultAsync(m => m.Id == orderId && m.Statues == "6");
            if (order != null)
            {
                order.Statues = "5";
                _context.Update(order);
                await _context.SaveChangesAsync();
                await Firebase.SendMessage(order.Client.Token, "استلم", "نجهز طلبك  برقم" + order.Id, "" + order.Id, "order");
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Moder Erorr";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> ChangOrderEnded(int orderId)
        {

            using (var transaction = _context.Database.BeginTransaction()) {
                try
                {
                    transaction.Commit();
                    ResponseBody responseBody = new ResponseBody();
                    var order = await _context.Orders.Include(m => m.Client).SingleOrDefaultAsync(m => m.Id == orderId && m.Statues == "4");
                    if (order != null && order.Amount > 0)
                    {



                        order.Statues = "1";
                        _context.Update(order);
                        await _context.SaveChangesAsync();

                        if (order.PayType == "Cash" || order.PayType == "cash")
                        {
                           await AddBlanceOrderToProvider(order.ProviderPhone,  order.Amount - order.AmountWithoutTax);

                        }



                        await Firebase.SendMessage(order.Client.Token, "استلم", "تم استلام الطلب برقم" + order.Id, "" + order.Id, "order");
                        responseBody.Message = "1";
                        return Ok(responseBody);
                    }
                    else
                    {
                        responseBody.Message = "Moder Erorr";
                        return Ok(responseBody);
                    }
                }
                catch (Exception x)
                {
                    ResponseBody responseBody = new ResponseBody();
                    transaction.Rollback();
                    return Ok(responseBody.Error = x.Message);
                }
            }
               
        }
        [HttpGet]
        public async Task<IActionResult> ProviderCancelOrder(int orderId)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var order = await _context.Orders.Include(m => m.Client).SingleOrDefaultAsync(m => m.Id == orderId);
            if (order != null)
            {
                if (order.Statues == "5" || order.Statues == "6")
                {
                    order.Statues = "2";
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                    await Firebase.SendMessage(order.Client.Token, "استلم", "قام المندوب بالغاء الطلب ", "" + order.Id, "order");
                    responseBody.Message = "1";
                    return Ok(responseBody);
                }
                {
                    responseBody.Message = "2";//not canceled
                    return Ok(responseBody);
                }
            }
            else
            {
                responseBody.Message = "Moder Erorr";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> ClientCancelOrder(int orderId)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
               var order = await _context.Orders.Include(m => m.Client).SingleOrDefaultAsync(m => m.Id == orderId);
            if (order != null)
            {
                if (order.Statues == "5" || order.Statues == "6" || order.Statues == "0")
                {
                    order.Statues = "3";
                    _context.Update(order);
                    await _context.SaveChangesAsync();

                    if(order.ProviderPhone != "0")
                    {
                    var provider = await _context.Providers.FindAsync(order.ProviderPhone);
                    await Firebase.SendMessage(provider.Token, "استلم", " قام العميل بالغاء الطلب رقم " + order.Id, "" + order.Id, "order");
                    }

                    responseBody.Message = "1";
                    return Ok(responseBody);
                }
                else
                {
                    responseBody.Message = "2";

                    return Ok(responseBody);//not canceled
                }
            }
            else
            {
                responseBody.Message = "Moder Erorr";

                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetDistrict()
        {
            try { 
            return Ok(await _context.Districts.OrderByDescending(m => m.Id).ToListAsync());
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetDistrictByCity(string city)    
        {
            try { 
            return Ok(await _context.Districts.OrderByDescending(m => m.DistrictName).Where(s=>s.City == city).ToListAsync());
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
         
        [HttpGet]
        public async Task<IActionResult> GetCity()
        {
            try { 
            List< District> allData = await _context.Districts.OrderByDescending(m => m.Id).ToListAsync();
            List<string> vs = new List<string>();
            foreach (var item in allData.OrderBy(m=>m.City))
            {
                if (!vs.Contains(item.City) && (item.City == "مكة المكرمة" || item.City == "الرياض"|| item.City == "جدة"|| item.City == "الدمام"
                        || item.City == "الخبر" || item.City == "الهفوف" || item.City == "الظهران" || item.City == "القطيف" || item.City == "الاحساء" ||  item.City == "الجبيل"))   
                    vs.Add(item.City);
            }
            return Ok(vs);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetOfferPrice(int orderId)
        {
            try { 
            var offerData = await _context.OfferPrices.Include(m => m.Order).Include(p => p.Provider).Where(m => m.OrderId == orderId).ToListAsync();
            List<OfferPriceViewModel> offerPriceViewModels = new List<OfferPriceViewModel>();
            foreach (var item in offerData)
            {
                OfferPriceViewModel offerPriceViewModel = new OfferPriceViewModel();
                var valuations = _context.ValuationProviders.Where(m => m.ProviderPhone == item.ProviderPhone);
                double valuationSum = 0; valuationSum = valuations.Sum(m => m.Value);


                double valuationCount = 0; valuationCount = valuations.Count();
                if (valuationCount > 0 && valuationSum > 0) { offerPriceViewModel.ProviderValuation = valuationSum / valuationCount; } else { offerPriceViewModel.ProviderValuation = 0; }

                offerPriceViewModel.OrderId = item.Order.Id;
                offerPriceViewModel.ProviderCarName = item.Provider.CarClass;
                offerPriceViewModel.ProviderCarPlateNumber = item.Provider.PlateNumber;
                offerPriceViewModel.ProviderPhone = item.ProviderPhone;
                offerPriceViewModel.ProviderImage = item.Provider.Image;
                offerPriceViewModel.OfferPrice = item.Offer;
                offerPriceViewModel.ProviderName = item.Provider.FullName;
                    
                offerPriceViewModel.UserLat = item.Order.UserLat;
                offerPriceViewModel.UserLong = item.Order.UserLong;
                offerPriceViewModel.OrderLat = item.Order.OrderLat;
                offerPriceViewModel.OrderLong = item.Order.OrderLong;
                offerPriceViewModels.Add(offerPriceViewModel);
            }

            return Ok(offerPriceViewModels);

            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOfferPrice([FromBody] OfferPrice offerPrice)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            if (ModelState.IsValid)
            {
                var offerPriceValue = await _context.OfferPrices.Include(i => i.Order).SingleOrDefaultAsync(m => m.OrderId == offerPrice.OrderId && m.ProviderPhone == offerPrice.ProviderPhone);
                if (offerPriceValue == null)
                {
                    _context.Add(offerPrice);
                    await _context.SaveChangesAsync();

                     var order = await _context.Orders.Include(a=>a.Client).SingleOrDefaultAsync(q=>q.Id == offerPrice.OrderId);
                    if (order != null) {await Firebase.SendMessage(order.Client.Token, "استلم", "هنالك عرض سعر جديد",""+ order.Id, "order");}
                    await ProviderRejectOrder(offerPrice.ProviderPhone);
                    responseBody.Message = "1";
                    return Ok(responseBody);
                }
                else
                {
                    responseBody.Message = "Exist";
                    return Ok(responseBody);
                }

            }

            else
            {
                responseBody.Message = "Model Error";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> ClientAcceptOfferPrice([FromBody] ClientAcceptOfferPriceViewModel clientAccptOfferPriceViewModel)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            if(ModelState.IsValid)
            {
                var order =await _context.Orders.FindAsync(clientAccptOfferPriceViewModel.OrderId);
                var provider = await _context.Providers.FindAsync(clientAccptOfferPriceViewModel.ProviderPhone);
                order.ProviderPhone = clientAccptOfferPriceViewModel.ProviderPhone;
                order.Statues = "6";
                _context.Update(order);
                await _context.SaveChangesAsync();
                responseBody.Message = "1";
                await Firebase.SendMessage(provider.Token, "استلم", "قام العميل بالموافقة علي سعرك بطلب رقم" + clientAccptOfferPriceViewModel.OrderId, "" + clientAccptOfferPriceViewModel.OrderId, "order");

                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
         
        [HttpGet] 
        public async Task<IActionResult> GetTrackingOrderData(string providerPhone ,int orderId) 
        {
            try { 
            var provider = await _context.Providers.Include(m => m.OfferPrices).SingleOrDefaultAsync(m => m.Phone == providerPhone);
            TrackingOrderDataViewModel offerPriceViewModel = new TrackingOrderDataViewModel();
            if (provider != null)
            {
                var order = await _context.Orders.FindAsync(orderId); 
                var offerprice = await _context.OfferPrices.SingleOrDefaultAsync(m=>m.ProviderPhone == providerPhone && m.OrderId == orderId);
                var valuations = _context.ValuationProviders.Where(m => m.ProviderPhone == provider.Phone);
                double valuationSum = 0; valuationSum = valuations.Sum(m => m.Value);
                double valuationCount = 0; valuationCount = valuations.Count();
                if (valuationCount > 0 && valuationSum > 0) { offerPriceViewModel.ProviderValuation = valuationSum / valuationCount; } else { offerPriceViewModel.ProviderValuation = 0; }

                offerPriceViewModel.UserLat= order.UserLat;
                offerPriceViewModel.OrderLat=order.OrderLat;
                offerPriceViewModel.OrderLong=order.OrderLong;
                offerPriceViewModel.UserLong=order.UserLong;
                offerPriceViewModel.ClientPhone = order.ClientPhone;
                offerPriceViewModel.ProviderName = provider.FullName;
                offerPriceViewModel.ProviderCarName = provider.CarClass;
                offerPriceViewModel.ProviderCarPlateNumber = provider.PlateNumber;
                offerPriceViewModel.ProviderPhone = provider.Phone;
                offerPriceViewModel.ProviderImage = provider.Image;
                offerPriceViewModel.OfferPrice = offerprice.Offer;
            }
                return Ok(offerPriceViewModel);

            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetDataDirectionToClient(string providerPhone, int orderId)    
        {
            try { 
            var provider = await _context.Providers.Include(m => m.OfferPrices).SingleOrDefaultAsync(m => m.Phone == providerPhone);
            TrackingOrderDataViewModel offerPriceViewModel = new TrackingOrderDataViewModel();
            if (provider != null)
            {
                var order = await _context.Orders.Include(c=>c.Client).SingleOrDefaultAsync(m=>m.Id == orderId);

                var offerprice = await _context.OfferPrices.SingleOrDefaultAsync(m => m.ProviderPhone == providerPhone && m.OrderId == orderId);
                var valuations = _context.ValuationProviders.Where(m => m.ProviderPhone == provider.Phone);
                double valuationSum = 0; valuationSum = valuations.Sum(m => m.Value);
                double valuationCount = 0; valuationCount = valuations.Count();
                if (valuationCount > 0 && valuationSum > 0) { offerPriceViewModel.ProviderValuation = valuationSum / valuationCount; } else { offerPriceViewModel.ProviderValuation = 0; }
                offerPriceViewModel.ClientImage = order.Client.Image;
                offerPriceViewModel.ClientName = order.Client.FullName;
                offerPriceViewModel.ClientAddress = order.Client.City + " - " + order.Client.District;
                offerPriceViewModel.ClientPhone = order.Client.Phone;
                offerPriceViewModel.ProviderName = provider.FullName;
                offerPriceViewModel.ProviderCarName = provider.CarClass;
                offerPriceViewModel.ProviderCarPlateNumber = provider.PlateNumber;
                offerPriceViewModel.ProviderPhone = provider.Phone;
                offerPriceViewModel.ProviderImage = provider.Image;
                offerPriceViewModel.OfferPrice = offerprice.Offer;
                offerPriceViewModel.UserLat = order.UserLat;
                offerPriceViewModel.OrderLat = order.OrderLat;
                offerPriceViewModel.OrderLong = order.OrderLong;
                offerPriceViewModel.UserLong = order.UserLong;
            }
            return Ok(offerPriceViewModel);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderDetails(int orderId)
        {
            try { 
            return Ok(await _context.Orders.Include(m=>m.Client).SingleOrDefaultAsync(m=>m.Id == orderId));
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetDelivery(int orderId)   
        {
          
            return Ok(await GetDeliveryPrice(orderId));
        }

        [HttpGet]
        public async Task<ResponseBody> GetDeliveryPrice(int orderId)
        {
            ResponseBody responseBody = new ResponseBody();
            var order = await _context.Orders.FindAsync(orderId);
            if (order != null)
                if (order.ProviderPhone.Length > 9)
                {
                    var offerPrice = await _context.OfferPrices.SingleOrDefaultAsync(m => m.OrderId == orderId && m.ProviderPhone == order.ProviderPhone);
                    responseBody.Message = offerPrice.Offer;
                }
                else
                {
                    responseBody.Message = "0";
                }
            return responseBody;
        }


        [HttpGet]
        public async Task<IActionResult> AddBlanceOrderToProvider(string providerPhone, double amount)
        {

            ProviderAccount providerAccount = new ProviderAccount();
            providerAccount.Amount = amount * -1;
            providerAccount.DateTimeNow = DateTime.Now;
            providerAccount.ProviderPhone = providerPhone;
            providerAccount.Type = "-";
            _context.Add(providerAccount);
            await _context.SaveChangesAsync(); 
            return Ok("1");
        } 


        [HttpPost]
        public async Task<IActionResult> AddPriceToOrder([FromBody] AddPriceToOrderViewModel addPriceToOrderViewModel)  
        {
            ResponseBody responseBody = new ResponseBody();

            ResponseBody delivery = await GetDeliveryPrice(addPriceToOrderViewModel.OrderId);  
           
            if (ModelState.IsValid)
            {
                Order order =await _context.Orders.Include(m=>m.Client).SingleOrDefaultAsync(m=>m.Id == addPriceToOrderViewModel.OrderId);

                double orderAmount = addPriceToOrderViewModel.Price - Double.Parse(delivery.Message.ToString());
                if (order.Amount == 0)
                {
                    order.AmountWithoutTax = addPriceToOrderViewModel.Price;

                    if ((order.PayType == "Cash" || order.PayType == "cash" ) && orderAmount >= 300)
                    {
                        order.Amount = addPriceToOrderViewModel.Price ;// بالنسبة رسوم الخدمة ريال 
                    }
                    else
                    {
                    order.Amount = (addPriceToOrderViewModel.Price * 0.029) + addPriceToOrderViewModel.Price;// بالنسبة رسوم الخدمة ريال 
                    }
                    _context.Update(order);




                    await _context.SaveChangesAsync();
                  
                    await Firebase.SendMessage(order.Client.Token, "استلم", "فاتورة الطلب برقم " + order.Id + " \n قيمة التوصيل :"+ delivery.Message  + " \n  قيمة الطلب شامل التوصيل ورسوم الخدمة :" + (addPriceToOrderViewModel.Price +(addPriceToOrderViewModel.Price * .029)), ""+order.Id, "order");

  
                    HttpGETPOST.SendInvoiceToChat(order.ClientPhone,order.ProviderPhone, "فاتورة الطلب برقم " 
                        + order.Id + " \n قيمة التوصيل :" + delivery.Message 

                         +" \n  قيمة الطلب   :" + orderAmount +


                          " \n  قيمة الطلب شامل التوصيل ورسوم الخدمة :" + (addPriceToOrderViewModel.Price  + (addPriceToOrderViewModel.Price * 0.029)) +"\n \n الإدارة");
 
   
                responseBody.Message = "1";
                }else
                {
                    responseBody.Message = "Model Error";
                }

            }
            else
            {
                responseBody.Message = "Model Error";
            }
            return Ok(responseBody);
        }
    }
}















//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ providers.Count  );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item.Phone  );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. FullName );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. Image );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. City );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item.  CarClass);
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item.PlateNumber  );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. NationalCardImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. DrivinglicenseImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. FormImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. DrivinglicenseAuthoImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. BackCarImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. FrontCarImage );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. Statues );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. Connectivity );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. Log );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item.  Lat);
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. Token );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. ConnectionId );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. ExpirationDate );
//Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+ item. DistrictId );