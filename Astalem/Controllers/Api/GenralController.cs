﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astalem;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Astalem.Models.AstalemViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Astalem.Controllers.Api   
{
    [Produces("application/json")]
    [Route("api/Genral/[Action]")]
    public class GenralController : Controller
    {

        private readonly ApplicationDbContext _context;
        public GenralController(ApplicationDbContext Context)
        {
            _context = Context;
        }
        [HttpGet]
        public async Task<IActionResult> App()
        {
            try { 
            return Ok(await _context.App.FirstOrDefaultAsync(m=>m.Id>0));
            }
            catch (Exception x)
            {
                return Ok();
            }
        }


        public async Task<IActionResult> city() 
        {
            return Ok(await _context.Districts.ToListAsync());
        }

        //[HttpGet]
        //public async Task<IActionResult> TestReciveMessageFromFirebse(string clientPhone)     
        //{
        //    try { 
        //    var client = await _context.Clients.FindAsync(clientPhone);
        //    await Firebase.SendMessage(client.Token,"العنوان","محتوي الرسالة","1","order");
        //    return Ok(client.Token);
        //    }
        //    catch (Exception x)
        //    {
        //        return Ok();
        //    }
        //}


        [HttpPost]
        public async Task<IActionResult> AddSuggest([FromBody] Proposals  proposals)   
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            if(ModelState.IsValid)
            {
                _context.Add(proposals);
                await _context.SaveChangesAsync();
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";
            return Ok(responseBody);

            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

//        public async Task<IActionResult>  m()
//        {
//            List<List<District>> end = new List<List<District>> ();
//            var cit = new List<District>();
//            List<string> dada = new List<string> {
//            "القطيف",
//"سيهات",
//"الخفجي",
//"بريدة",
//"حايل",
//"بقيق",
//"النعيرية",
//"صفوى",
//"المجمعة",
//"شقراء",
//"رابغ",
//"الدوادمي",
//"المبرز",
//"ابوعريش",
//"الزلفي",
//"طريف",
//"القريات",
//"رفحة",
//"بيشة",
//"القنفذة",
//"تاروت",
//"عرعر",
//"النماص",
//"رياض الخبراء",
//"احد رفيدة",
//"البكيرية",
//"المذنب",
//"عفيف",
//"المزاحمية",
//"العويلقية",
//"قرية العليا",
//"الدلم",
//"حوطة بني تميم",
//"القويعية",
//"البدائع",
//"محايل عسير",
//"خليص",
//"شرورة",
//"الاحساء",
//"عنك",
//"دومة الجندل",
//"بلجرشي",
//"العلا",
//"املج",
//"وادي الدواسر",
//"صبيا",
//"الوجة",
//"خيبر","سكاكا"};
//            foreach (var items in dada)
//            {
//             cit = _context.Districts.Where(m => m.City == items).ToList();
//            foreach (var item in cit)   
//            {
//                item.DistrictName = " "+"مندوب" +" "+ item.DistrictName;

//                _context.Update(item);
//            }
//                end.Add(cit);
//            }
//        //   await   _context.SaveChangesAsync();

//            return Ok(end);
//        }
        //public IActionResult t()
        //{
        //    HttpGETPOST.SendSMS("0532418301", "i  u");
        //    return Ok("1");
        //}
    }
}