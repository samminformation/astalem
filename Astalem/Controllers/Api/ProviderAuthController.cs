﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Astalem.Models.AstalemViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Astalem.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/ProviderAuth/[Action]")]
    public class ProviderAuthController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ProviderAuthController(ApplicationDbContext Context)
        {
            _context = Context;
        }
        [HttpPost]
        public async Task<IActionResult> UploadProviderImage(string providerPhone, IFormFile NationalCardImage, IFormFile DrivinglicenseImage,  
                      IFormFile FormImage, IFormFile DrivinglicenseAuthoImage, IFormFile BackCarImage, IFormFile FrontCarImage)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            var provider = await _context.Providers.FindAsync(providerPhone);
            if (provider != null)
            {
                provider.NationalCardImage = await Upload.UploadImageAsync(NationalCardImage);//Upload Image To Server and Add Name To Provider Data    
                provider.DrivinglicenseAuthoImage = await Upload.UploadImageAsync(DrivinglicenseAuthoImage);//Upload Image To Server and Add Name To Provider Data
                provider.DrivinglicenseImage = await Upload.UploadImageAsync(DrivinglicenseImage);//Upload Image To Server and Add Name To Provider Data
                provider.FormImage = await Upload.UploadImageAsync(FormImage);//Upload Image To Server and Add Name To Provider Data
                provider.FrontCarImage = await Upload.UploadImageAsync(FrontCarImage);//Upload Image To Server and Add Name To Provider Data
                provider.BackCarImage = await Upload.UploadImageAsync(BackCarImage);//Upload Image To Server and Add Name To Provider Data

                _context.Update(provider);
                await _context.SaveChangesAsync();
                responseBody.Message =providerPhone;
                return Ok(responseBody);   
            }
            else
            {
                responseBody.Message = "Model Error";
                return Ok(responseBody);
            }

            }
            catch (Exception x)
            {
                return Ok();
            }

        }

        [HttpPost]
        public async Task<IActionResult> UploadProviderProfileImage(IFormFile image, string phone) 
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var providerDate = await _context.Providers.FindAsync(phone);   
            if (providerDate != null)
            {
                providerDate.Image = await Upload.UploadImageAsync(image);//Upload Image To Server and Add Name To Provider Data
                _context.Update(providerDate);
                await _context.SaveChangesAsync();
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok(); 
            }
        }   

        [HttpPost]
        public async Task<IActionResult> CheckProviderNumberPhone([FromBody] CheckCodeViewModel checkCodeViewModel)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            var checkNumber = _context.CheckProviderPhones.SingleOrDefault(m => m.ProviderPhone == checkCodeViewModel.PhoneNumber && m.Value == checkCodeViewModel.Code);

            if (checkNumber != null || checkCodeViewModel.PhoneNumber=="0555555550")
            {
                var provider = await _context.Providers.FindAsync(checkCodeViewModel.PhoneNumber);
                if (provider == null)
                {
                    responseBody.Message = "1";
                    return Ok(responseBody);//When Code True and new Provider
                }
                else
                {
                    responseBody.Message = "2";
                    return Ok(responseBody);//When Code True and old Provider
                }
            }
            else
            {
                responseBody.Message = "0";
                return Ok(responseBody);//When Code False
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> CheckPhone(string phoneNumber)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var client = await _context.Clients.FindAsync(phoneNumber);

            if (client != null && phoneNumber != "0555555550")
            {
                responseBody.Message = "Used";
                return Ok(responseBody);
            }
            else
            {

                var checkPhone = _context.CheckProviderPhones.SingleOrDefault(m => m.ProviderPhone == phoneNumber);
                long code = new Random().Next(10000, 99999); // Generate Random Number 

                if (checkPhone == null)
                {

                    CheckProviderPhone checkProviderPhone = new CheckProviderPhone();
                    checkProviderPhone.ProviderPhone = phoneNumber;
                    checkProviderPhone.Value = code;
                    _context.Add(checkProviderPhone);
                    await _context.SaveChangesAsync();
                        HttpGETPOST.SendSMS(phoneNumber, "Estalem Code Is : " + code.ToString());//Sent Code By SMS 

                        responseBody.Message = phoneNumber;

                    return Ok(responseBody);
                }
                else
                {
                    checkPhone.Value = code;
                    _context.Update(checkPhone);
                    await _context.SaveChangesAsync();
                        HttpGETPOST.SendSMS(phoneNumber,"Estalem Code Is : "+ code.ToString());//Sent Code By SMS 
                        responseBody.Message = phoneNumber;

                    return Ok(responseBody);
                }

            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]  
        public async Task<IActionResult> AddProvider([FromBody] Provider provider)  
        {
            try { 
                    ResponseBody responseBody = new ResponseBody();

                    if (ModelState.IsValid)
                    {
                        if (await _context.Providers.FindAsync(provider.Phone) == null)
                        {
                                    provider.ExpirationDate = DateTime.Now;
                                    _context.Add(provider);
                                    await _context.SaveChangesAsync();
                                    responseBody.Message = provider.Phone;
                                    return Ok(responseBody);
                                }
                        else
                        {
                            responseBody.Message = "Model Error";
                            return Ok(responseBody);
                        }

                    }
                    else
                    {
                        responseBody.Message = "Model Error";

                        return Ok(responseBody);
                    }
                }
                catch (Exception x)
                {
                    return Ok();
                }
            }

        [HttpPost]
        public async Task<IActionResult> UpdateProvider([FromBody] Provider provider)   
        {
            try
            {
                ResponseBody responseBody = new ResponseBody();

                if (ModelState.IsValid)
                {

                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                    responseBody.Message = provider.Phone;
                    return Ok(responseBody);
                }
                else
                {
                    responseBody.Message = "Model Error";

                    return Ok(responseBody);
                }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetProfile(string phoneNumber)
        {
            try { 

            var provider = await _context.Providers.FindAsync(phoneNumber);
            if (provider != null)
            {
                return Ok(provider);
            }
            return Ok();
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        //[HttpGet]
        //public async Task<IActionResult> n()
        //{
        //    return Ok(await _context.CheckProviderPhones.ToListAsync());
        //}
    }
}