﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Microsoft.AspNetCore.Authorization;

namespace Astalem.Controllers.Api
{
    [Authorize]
    public class ProvidersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProvidersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Providers
        public async Task<IActionResult> Index(string phone)
        {
            var applicationDbContext = _context.Providers.Include(p => p.District);
            return View(await applicationDbContext.Where(m=>m.Phone == phone).ToListAsync());
        }

        // GET: Providers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .Include(p => p.District)
                .SingleOrDefaultAsync(m => m.Phone == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // GET: Providers/Create
        public IActionResult Create()
        {
            ViewData["DistrictId"] = new SelectList(_context.Districts, "Id", "City");
            return View();
        }

        // POST: Providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Phone,FullName,Image,City,IPanNumber,BankName,BankAccountName,CarClass,PlateNumber,NationalCardImage,DrivinglicenseImage,FormImage,DrivinglicenseAuthoImage,BackCarImage,FrontCarImage,Statues,Connectivity,Log,Lat,Token,ConnectionId,ExpirationDate,DistrictId")] Provider provider)
        {
            if (ModelState.IsValid)
            {
                _context.Add(provider);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DistrictId"] = new SelectList(_context.Districts, "Id", "City", provider.DistrictId);
            return View(provider);
        }

        // GET: Providers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers.SingleOrDefaultAsync(m => m.Phone == id);
            if (provider == null)
            {
                return NotFound();
            }
            ViewData["DistrictId"] = new SelectList(_context.Districts, "Id", "City", provider.DistrictId);
            return View(provider);
        }

        // POST: Providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Phone,FullName,Image,City,IPanNumber,BankName,BankAccountName,CarClass,PlateNumber,NationalCardImage,DrivinglicenseImage,FormImage,DrivinglicenseAuthoImage,BackCarImage,FrontCarImage,Statues,Connectivity,Log,Lat,Token,ConnectionId,ExpirationDate,DistrictId")] Provider provider)
        {
            if (id != provider.Phone)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProviderExists(provider.Phone))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DistrictId"] = new SelectList(_context.Districts, "Id", "City", provider.DistrictId);
            return View(provider);
        }

        // GET: Providers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .Include(p => p.District)
                .SingleOrDefaultAsync(m => m.Phone == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // POST: Providers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var provider = await _context.Providers.SingleOrDefaultAsync(m => m.Phone == id);
            _context.Providers.Remove(provider);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProviderExists(string id)
        {
            return _context.Providers.Any(e => e.Phone == id);
        }
    }
}
