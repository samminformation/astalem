﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astalem.Data;
using Astalem.Models.AstalemViewModels;
using Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace Astalem.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Client/[Action]")]
    public class ClientController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHubContext<MainHub> _hubcontext { get; set; }
        public ClientController(ApplicationDbContext Context, IHubContext<MainHub> Hubcontext)
        {
            _context = Context;
            _hubcontext = Hubcontext;
        }
        [HttpGet]
        public async Task<IActionResult> GetClientOrders(string clientPhone)
        {
            try { 
            return Ok(await _context.Orders.Where(m => m.ClientPhone == clientPhone).OrderByDescending(m => m.Id).ToListAsync());
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public IActionResult ClientAccountAmount(string clientPhone)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            responseBody.Message = Convert.ToString(_context.ClientAccounts.Where(m => m.ClientPhone == clientPhone).ToList().Sum(m => m.Amount));
            return Ok(responseBody);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<double> GetCoboneValue(string cobonCode)
        {
            try { 
            var cobone = await _context.Cobones.SingleOrDefaultAsync(m => m.CoboneCode == cobonCode);
                if (cobone == null)
                {
                    return 0;
                }
                else
                {
                    return cobone.CoboneValue;
                }
            }
            catch (Exception x)
            {
                return 0;
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateClientToken([FromBody] UpdateTokenViewModel updateTokenViewModel)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            if (ModelState.IsValid) 
            {
                var client =await _context.Clients.FindAsync(updateTokenViewModel.UserPhone);
                if(client!=null)
                {
                    client.Token = updateTokenViewModel.Token;
                    _context.Update(client);
                    await _context.SaveChangesAsync();
                }
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";

                return Ok("Model Error");
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }   

    }
}

