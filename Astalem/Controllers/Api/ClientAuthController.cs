﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Astalem.Models.AstalemViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Astalem.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/ClientAuth/[action]")]
    public class ClientAuthController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ClientAuthController(ApplicationDbContext Context)
        {
            _context = Context;
        } 

        [HttpPost]
        public async Task<IActionResult> UploadClientImage(IFormFile image, string phone)
        {
            try
            {
                ResponseBody responseBody = new ResponseBody();
                var clientDate = await _context.Clients.FindAsync(phone);
                if (clientDate != null)
                {
                    clientDate.Image = await Upload.UploadImageAsync(image);//Upload Image To Server and Add Name To Client Data
                    _context.Update(clientDate);
                    await _context.SaveChangesAsync();
                    responseBody.Message = "1";
                    return Ok(responseBody);
                }
                else
                {
                    responseBody.Message = "Model Error";
                    return Ok(responseBody);
                }
            }catch(Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> CheckClientNumberPhone([FromBody] CheckCodeViewModel checkCodeViewModel)  
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            var checkNumber = _context.CheckClientPhones.SingleOrDefault(m => m.ClientPhone == checkCodeViewModel.PhoneNumber && m.Value == checkCodeViewModel.Code);

            if (checkNumber != null || checkCodeViewModel.PhoneNumber == "0555555550")
            {
               var client = await _context.Clients.FindAsync(checkCodeViewModel.PhoneNumber);
                if(client ==null)
                {
                    responseBody.Message = "1";
                    return Ok(responseBody);//When Code True and new client
                }
                else
                {
                    responseBody.Message = "2";
                    return Ok(responseBody);//When Code True and old client
                }
            }
            else
            {
                responseBody.Message = "0";
                return Ok(responseBody);//When Code False
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> CheckPhone(string phoneNumber)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var provider = await _context.Providers.FindAsync(phoneNumber);

            if (provider != null && phoneNumber != "0555555550")
            {
                responseBody.Message = "Used";
                return Ok(responseBody);
            }
            else
            { 
            var checkPhone = _context.CheckClientPhones.SingleOrDefault(m => m.ClientPhone == phoneNumber);
                long code = new Random().Next(10000, 99999); // Generate Random Number 

                if (checkPhone == null)
                {

                    CheckClientPhone checkClientPhone = new CheckClientPhone();
                    checkClientPhone.ClientPhone = phoneNumber;
                    checkClientPhone.Value = code;
                    _context.Add(checkClientPhone);
                    await _context.SaveChangesAsync();
                        HttpGETPOST.SendSMS(phoneNumber, "Estalem Code Is : " + code.ToString());//Sent Code By SMS 
                        responseBody.Message = phoneNumber;

                return Ok(responseBody);
                }
                else
                {
                    checkPhone.Value = code;
                    _context.Update(checkPhone);
                    await _context.SaveChangesAsync();
                        HttpGETPOST.SendSMS(phoneNumber, "Estalem Code Is : " + code.ToString());//Sent Code By SMS 
                        responseBody.Message = phoneNumber;

                return Ok(responseBody);
                }
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddClient([FromBody] Client client)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            if (ModelState.IsValid)
            {
                if(await _context.Clients.FindAsync(client.Phone) ==null)
                {
                _context.Add(client);
                await _context.SaveChangesAsync();
                responseBody.Message = client.Phone;
                return Ok(responseBody);
                }
                else
                {
                    responseBody.Message = "Model Error";
                    return Ok(responseBody);
                }

            }
            else
            {
                responseBody.Message = "Model Error";

                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateClient([FromBody] Client client) 
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            if (ModelState.IsValid)
            {

                _context.Update(client);
                await _context.SaveChangesAsync();
                responseBody.Message = client.Phone;
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";

                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetProfile(string phoneNumber) 
        {
            try { 
            var client = await _context.Clients.FindAsync(phoneNumber);
            if(client != null)
            {
                return Ok(client);  
            }
            return Ok();
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        //[HttpGet]
        //public async Task<IActionResult> n()  
        //{        
        //        return Ok(await _context.CheckClientPhones.ToListAsync());           
        //}

    }
}