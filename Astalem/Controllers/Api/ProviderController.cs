﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astalem.Data;
using Astalem.Models.AstalemModel;
using Astalem.Models.AstalemViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;



namespace Astalem.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Provider/[Action]")]
    public class ProviderController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ProviderController(ApplicationDbContext Context)
        {
            _context = Context;
        } 
        [HttpPost]
        public async Task< IActionResult> UpdateLocation([FromBody]UpdateLocationViewModel updateLocationViewModel)
        {
            try { 
            if(ModelState.IsValid)
            {
                var provider = await _context.Providers.FindAsync(updateLocationViewModel.ProviderPhone);
                if(provider!= null)
                {
                    provider.Lat = updateLocationViewModel.Lat;
                    provider.Log = updateLocationViewModel.Log;
                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                }
                return Ok("1");
            }
            else
            {
                return Ok("Error Model");
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> SetActive(string providerPhone)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            var provider = await _context.Providers.FindAsync(providerPhone);
            if (provider != null)
            {
                provider.Connectivity = "1";
                _context.Update(provider);
                    await ProviderRejectOrder(providerPhone);
                    await _context.SaveChangesAsync();
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Error Model";
                return Ok(responseBody);

            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public async Task<IActionResult> SetNotActive(string providerPhone)     
        {
            try { 
            ResponseBody responseBody = new ResponseBody();

            var provider = await _context.Providers.FindAsync(providerPhone);
            if (provider != null)
            {

                provider.Connectivity = "0";
                _context.Update(provider);
                await _context.SaveChangesAsync();
                await  ProviderRejectOrder(providerPhone);
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Error Model";
                return Ok(responseBody);

            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }


        [HttpGet]
        public async Task<IActionResult> ProviderRejectOrder(string providerPhone)
        {
            try
            {


                ResponseBody responseBody = new ResponseBody();
                var orderState = await _context.OrderStates.Where(m => m.ProviderPhone == providerPhone).ToListAsync();
                if (orderState != null)
                {
                    _context.RemoveRange(orderState);
                    await _context.SaveChangesAsync();
                }
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }


        [HttpGet]
        public async Task<IActionResult> ProviderOrders(string providerPhone)        
        {
            try { 
            return Ok(await _context.Orders.Where(m=>m.ProviderPhone== providerPhone).OrderByDescending(m => m.Id).ToListAsync());
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProviderToken([FromBody] UpdateTokenViewModel updateTokenViewModel)  
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            if (ModelState.IsValid)
            {
                var provider = await _context.Providers.FindAsync(updateTokenViewModel.UserPhone); 
                if (provider != null)
                {
                    provider.Token = updateTokenViewModel.Token;
                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                }
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";

                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> ValuationProvider([FromBody] ValuationProvider valuationProvider)
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
            List<ValuationProvider> valuationProvide = await _context.ValuationProviders.Where(m => m.OrderId == valuationProvider.OrderId && m.ProviderPhone == valuationProvider.ProviderPhone).ToListAsync();
            if (ModelState.IsValid)
            {
                if (valuationProvide.Count<= 0)
                {
                    _context.Add(valuationProvider);
                    await _context.SaveChangesAsync();
                }
                responseBody.Message = "1";
                return Ok(responseBody);
            }
            else
            {
                responseBody.Message = "Model Error";
                return Ok(responseBody);
            }
            }
            catch (Exception x)
            {
                return Ok();
            }
        }

        [HttpGet]
        public IActionResult ProviderAccountAmount(string providerPhone) 
        {
            try { 
            ResponseBody responseBody = new ResponseBody();
          double x = _context.ProviderAccounts.Where(m => m.ProviderPhone == providerPhone).ToList().Sum(m => m.Amount);
            responseBody.Message = Convert.ToString(x+"");
            return Ok(responseBody);
            }
            catch (Exception x)
            {
                return Ok();
            }
        }
    }  
}