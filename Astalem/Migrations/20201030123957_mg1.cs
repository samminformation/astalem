﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Astalem.Migrations
{
    public partial class mg1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BankAccountName",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankName",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IPanNumber",
                table: "Providers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankAccountName",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "BankName",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "IPanNumber",
                table: "Providers");
        }
    }
}
