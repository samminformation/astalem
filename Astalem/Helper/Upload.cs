﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Data
{
    public static class Upload
    {
        public static async Task<string> UploadImageAsync(IFormFile image)
        {
            if (image != null && image.Length > 0)
            {

                var fileName = Path.GetFileName(image.FileName);
                fileName = Guid.NewGuid().ToString();

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload", fileName + ".png");


                using (var fileSteam = new FileStream(filePath, FileMode.Create))
                {
                    await image.CopyToAsync(fileSteam);
                }
                return  fileName + ".png"; ;

            }
            else
            {
                return "No Image";
            }

        }
    }
}
