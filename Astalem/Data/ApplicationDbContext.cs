﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Astalem.Models;
using Astalem.Models.AstalemModel;

namespace Astalem.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public DbSet<App> App { get; set; } 
        public DbSet<CheckClientPhone> CheckClientPhones { get; set; }
        public DbSet<CheckProviderPhone> CheckProviderPhones { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientAccount> ClientAccounts { get; set; }
        public DbSet<Cobone> Cobones { get; set; }  
        public DbSet<District> Districts { get; set; }
        public DbSet<OfferPrice> OfferPrices { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderState> OrderStates { get; set; }
        public DbSet<Proposals> Proposals { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<ProviderAccount> ProviderAccounts { get; set; }
        public DbSet<ProviderActiveHistory> ProviderActiveHistories { get; set; }   
        public DbSet<ValuationProvider> ValuationProviders { get; set; }    
    }
}
