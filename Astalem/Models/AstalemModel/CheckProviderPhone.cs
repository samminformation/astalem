﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class CheckProviderPhone
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "رقم التحقق")]
        public long Value { get; set; }

        [Required]
        [Display(Name = "هاتف المقدم")]
        public string ProviderPhone { get; set; }   
    }
}
