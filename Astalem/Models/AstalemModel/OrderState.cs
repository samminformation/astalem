﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class OrderState
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "حالة الطلب")]
        public int Stateus { get; set; }

        [Required]
        [Display(Name = "رقم الطلب")]
        public int OrderId { get; set; }
        public Order Order { get; set; }


        [Required]
        [Display(Name = "رقم العميل")]
        public string ClientPhone { get; set; }     
        public Client Client { get; set; }


        [Required]
        [Display(Name = "رقم المقدم")]
        public string ProviderPhone { get; set; }    
        public Provider Provider { get; set; }
    }
}
