﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class Cobone
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name ="كود الخصم")]
        public string CoboneCode { get; set; }
        [Required]
        [Display(Name = "قيمة الخصم>")]
        public double CoboneValue { get; set; }
        [Required]
        [Display(Name = "الحالة")]
        public string State { get; set; }

        [Required]
        [Display(Name = "زمن انتهاء الكوبون")]
        public DateTime ExpirationDate { get; set; }
        //[Required]
        //[Display(Name = "اسم العميل")]
        //public int ClientId { get; set; }
        //public Client Client { get; set; }
    }
}
