﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class OfferPrice
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name ="عرض السعر")]
        public double Offer { get; set; }   


        [Required]
        [Display(Name = "رقم الطلب")]
        public int OrderId { get; set; }
        public Order Order { get; set; }


        [Required]
        [Display(Name = "اسم المقدم")]
        public string ProviderPhone { get; set; }    
        public Provider Provider { get; set; }
    }
}
