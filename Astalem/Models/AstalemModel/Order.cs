﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class Order
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "تاريخ انشاء الطلب")]
        public DateTime DateTimeNow { get; set; }
        [Required]
        [Display(Name = "السعر")]
        public double Amount { get; set; }

        [Required]
        [Display(Name = "خط عرض  المستخدم")]
        public double UserLat { get; set; }
        [Required]
        [Display(Name = "خط طول المستخدم")]
        public double UserLong { get; set; }


        [Required]
        [Display(Name = "وصف الطلب")]
        public string LocationDescription { get; set; }

        [Required]
        [Display(Name = "وصف موقع الطلب")]
        public string OrderDescription { get; set; }    




        [Required]
        [Display(Name = "خط عرض  الطلب")]
        public double OrderLat { get; set; }    
        [Required]
        [Display(Name = "خط طول الطلب")]
        public double OrderLong { get; set; }


        [Display(Name = "ملاحظة")]
        public string Note { get; set; }


        [Display(Name = "نوع الدفع")]
        public string PayType { get; set; }

        [Display(Name = "القيمة المدفوعة")]
        public double AmountPayed { get; set; }


        [Required]
        [Display(Name = "حالة الطلب")]
        public string Statues { get; set; }


        [Required]
        public string ProviderPhone { get; set; }

        [Display(Name = "الكوبون")]
        public double CobonValue { get; set; }

        [Display(Name = "المبلغ بدون خصب رسوم خدمة")]
        public double AmountWithoutTax { get; set; }
        //Relation  
        [Display(Name = "رقم العميل")]

        public string ClientPhone { get; set; }   
        public Client Client { get; set; }

        public  ICollection<OfferPrice> OfferPrices { get; set; }    
        public  ICollection<OrderState> OrderStates { get; set; }          

    }
}
