﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class ProviderAccount
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "المبلغ")]
        public double Amount { get; set; }
        [Required]
        [Display(Name = "التاريخ ")]
        public DateTime DateTimeNow { get; set; }
        [Required]
        [Display(Name = "النوع ")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "اسم المقدم")]
        public string ProviderPhone { get; set; }   
        public Provider Provider { get; set; }
    }
}
