﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class Proposals
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name ="اسم المقترح")]
        public string ProposalerName { get; set; }
        [Required]
        [Display(Name = "رقم هاتف المقترح")]
        public string ProposalerPhone { get; set; }
        [Required]
        [Display(Name = "عنوان الاقتراح")]
        public string ProposalerTitle { get; set; }
        [Required]
        [Display(Name = "محتوي الاقتراح")]
        public string ProposalerContain { get; set; }        
    }
}
