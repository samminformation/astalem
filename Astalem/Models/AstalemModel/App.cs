﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class App
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "حساب تويتر")]
        public string Twitter { get; set; }    

        [Required]
        [Display(Name = "حساب انستقرام")]
        public string Instagram { get; set; }


        [Required]
        [Display(Name = "حساب الفيس بوك")]
        public string Facebook { get; set; }
        [Required]
        [Display(Name = "رقم الاصدار")]
        public string Version { get; set; }



        [Required]
        [Display(Name = "ايميل ")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "رقم هاتف  ")]
        public string Phone { get; set; }   



        [Required]
        [Display(Name = "شروط وسياسات")]
        public string TermsAndPolicies { get; set; }

        [Required]
        [Display(Name = "حول")]
        public string About { get; set; }


        [Required]
        [Display(Name = "سعر الكيلومتر")]
        public string KMPrice { get; set; } 

        [Required]
        [Display(Name = "سعر بداية العداد")]
        public string CounterPrice { get; set; }
        [Required]
        [Display(Name = "رسوم تفعيل مندوب استلم")]
        public int AmountToActiveProvider { get; set; }
    }
}
