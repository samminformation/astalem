﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class Client 
    {
        [Key]
        [Required]
        [Display(Name = "رقم الهاتف")]
        public string Phone { get; set; }
        [Required]
        [Display(Name ="الاسم كامل")]
        public string FullName { get; set; }
        [Display(Name = "الصورة")]
        public string Image { get; set; }
        [Required]
        [Display(Name = "الحي او المنطقة")]
        public string District { get; set; }
        [Required]
        [Display(Name = "المدينة")]
        public string City { get; set; }
        [Display(Name = "خط الطول")]
        public double Log { get; set; }
        [Display(Name = "خط العرض")]
        public double Lat { get; set; }
        [Display(Name = "التوكن")]
        public string Token { get; set; }
        [Display(Name = "مفتاح الاتصال")]
        public string ConnectionId { get; set; }



        //relation
        public  ICollection<Order> Orders { get; set; }  
       // public virtual ICollection<CheckClientPhone> CheckClientPhones { get; set; }  
        public  ICollection<ClientAccount> ClientAccounts { get; set; }  
        public  ICollection<OrderState> OrderStates { get; set; }  


    }
}
