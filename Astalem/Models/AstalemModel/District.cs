﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class District
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "اسم الحي ")]
        public string DistrictName { get; set; }
        [Required]
        [Display(Name = "اسم المدينة")]
        public string City { get; set; }


        [Required]
        [Display(Name = "العرض خط ")]
        public double Lat { get; set; }

        [Required]
        [Display(Name = "الطول خط ")]
        public double Log { get; set; }


        public  ICollection<Provider> Providers { get; set; }    
    }
}
