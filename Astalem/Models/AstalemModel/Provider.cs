﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemModel
{
    public class Provider
    {
        [Key]
        [Required]
        [Display(Name = "رقم الهاتف")]
        public string Phone { get; set; }


        [Required]
        [Display(Name = "الاسم كامل")]
        public string FullName { get; set; }

        [Display(Name = "الصورة الشخصية")]
        public string Image { get; set; }


        [Required]
        [Display(Name = "المدينة")]
        public string City { get; set; }


        [Display(Name = "رقم الايبان ")]
        public string IPanNumber { get; set; }  

        [Display(Name = "اسم البنك")]
        public string BankName { get; set; }

        [Display(Name = "اسم صاحب البنك")]
        public string BankAccountName { get; set; } 



        [Required]
        [Display(Name = "فئة السيارة")]
        public string CarClass { get; set; }
        [Required]
        [Display(Name = "رقم اللوحة")]
        public string PlateNumber { get; set; }
        [Required]
        [Display(Name = "صورة الاقامة او الهوية")]
        public string NationalCardImage { get; set; }
        [Required]
        [Display(Name = "صورة الرخصة ")]
        public string DrivinglicenseImage { get; set; }
        [Required]
        [Display(Name = "صورة الاستمارة")]
        public string FormImage { get; set; }
        [Display(Name = "صورة تفويض القيادة ")]
        public string DrivinglicenseAuthoImage { get; set; }
        [Required]
        [Display(Name = "صورة المركبة الخلفية ")]
        public string BackCarImage { get; set; }
        [Required]
        [Display(Name = "صورة المركبة الامامية ")]
        public string FrontCarImage { get; set; }


        [Required]
        [Display(Name = "الحالة")]
        public string Statues { get; set; }
        [Display(Name = "متصل")]
        public string Connectivity { get; set; }
        [Display(Name = "خط الطول")]
        public double Log { get; set; }  
        [Display(Name = "خط العرض")]
        public double Lat { get; set; }
        [Display(Name = "التوكن")]
        public string Token { get; set; }
        [Display(Name = "مفتاح الاتصال")]
        public string ConnectionId { get; set; }
        [Display(Name = "تاريخ انتهاء التفعيل")]
        public DateTime ExpirationDate { get; set; }  


        //relation        
        [Required]
        [Display(Name = "الحي او المنطقة")]
        public int DistrictId { get; set; } 
        public District District { get; set; }


        public  ICollection<ValuationProvider> ValuationProviders { get; set; }
        public  ICollection<ProviderAccount> ProviderAccounts { get; set; }
        public  ICollection<OfferPrice> OfferPrices { get; set; }
        public  ICollection<OrderState> OrderStates { get; set; }
        public  ICollection<ProviderActiveHistory> ProviderActiveHistorys { get; set; } 

    }
}
