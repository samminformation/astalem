﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.ZajelViewMode
{
    public class ProviderAcceptOrderViewModel   
    {
        [Required]
        public string ProviderPhone { get; set; }   
        [Required]
        public int OrderId { get; set; }       
     
    }
}
