﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ProviderRecieveOrderDetilesViewModel
    {
        public int OrderId { get; set; }
        public string DescriptionLocation { get; set; } 
        public string DescriptionOrder { get; set; }    
        public string ClientPhone { get; set; }     
        public string ClientImage { get; set; } 
        public double DistanceOrderLocationFromClientLocation { get; set; }
        public double UserLat { get; set; }
        public double OrderLat { get; set; }
        public double OrderLong { get; set; }
        public double UserLong { get; set; }
    }
}