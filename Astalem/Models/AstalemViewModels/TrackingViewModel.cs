﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class TrackingViewModel
    {
        //public string ClientPhone { get; set; } 
        public string ProviderPhone { get; set; } 
        public double Lat { get; set; } 
        public double Log { get; set; }
    }
}
