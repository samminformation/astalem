﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ReportTimeViewModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }    
    }
}
