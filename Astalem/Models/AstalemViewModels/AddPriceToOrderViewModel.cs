﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class AddPriceToOrderViewModel
    {
        public int OrderId { get; set; }    
        public double Price { get; set; }       
    }
}
