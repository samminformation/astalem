﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class IndexViewModel
    {
        public int ClientCount { get; set; }
        public int ProviderCount { get; set; }  
        public int AllOrderCount { get; set; }  
        public int TodayOrderCount { get; set; }    
        public double TodayAmount { get; set; }    
        public double AllAmount { get; set; }        
        public  List<City> Cities { get; set; } 
    }
}
    public class City  
    {
    public string CityName { get; set; }
    public string AreaName { get; set; }    
    public int CountProviderInArea { get; set; }      
    }