﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class DataDirectionToClientViewModel
    {
        public string ClientName { get; set; }
        public string ClientImage { get; set; } 
        public string District { get; set; }
        public string City { get; set; }
    }
}
