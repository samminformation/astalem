﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ProviderReportViewModel
    {
        public string ProviderPhone { get; set; }
        public int OrderCount { get; set; } 
        public int TodayOrderCount { get; set; } 
        public string ProviderName { get; set; }   
        public double AllAmount { get; set; }       
        public double ProviderAmount { get; set; }           
        public string Area { get; set; }          
        public string City { get; set; }

        public string AccountNumber { get; set; }   
        public string AccountName { get; set; } 
        public string BankName { get; set; }    

    }
}