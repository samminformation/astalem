﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class TrackingOrderDataViewModel
    {
        public string ClientImage { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }   
        public string ClientPhone { get; set; } 
        public string ProviderName { get; set; }
        public string ProviderCarName { get; set; }
        public string ProviderCarPlateNumber { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderImage { get; set; }
        public double ProviderValuation { get; set; }
        public double OfferPrice { get; set; }
        public double UserLat { get; set; }
        public double OrderLat { get; set; }
        public double OrderLong { get; set; }
        public double UserLong { get; set; }
    }
}
