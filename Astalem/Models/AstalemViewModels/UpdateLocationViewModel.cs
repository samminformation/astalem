﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class UpdateLocationViewModel
    {
        [Required]
        public string ProviderPhone { get; set; }   
        [Required]
        public double Lat { get; set; }
        [Required]
        public double Log { get; set; } 
    }
}
