﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class OrderReportViewModel
    {
        public DateTime From { get; set; }  
        public DateTime To { get; set; }  
        public int OrderCount { get; set; }     
       // public string MaxProviderOrderCount { get; set; }      
        public double AllAmount { get; set; }
        public int OrderCashCount { get; set; }
        public int OrderOnLineCount { get; set; }   
        public int OrderInRoadCount { get; set; }
        public int OrderInAttendCount { get; set; }
        public int OrderFinshedCount { get; set; }
        public int OrderCanceledCount { get; set; } 

    }
}
