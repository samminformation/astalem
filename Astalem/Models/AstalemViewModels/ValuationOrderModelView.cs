﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ValuationOrderModelView
    {
        public int orderId { get; set; }
        public double productValuation { get; set; }
        public double timeValuation { get; set; }
        public double providerValuation { get; set; }   
    }
}
