﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ResponseBody
    {
        public object Message { get; set; }
        public object Error { get; set; }   
    }
}
