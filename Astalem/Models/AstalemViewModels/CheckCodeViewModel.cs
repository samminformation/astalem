﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class CheckCodeViewModel
    {
        public long Code { get; set; }  
        public string PhoneNumber { get; set; }     
    }
}
