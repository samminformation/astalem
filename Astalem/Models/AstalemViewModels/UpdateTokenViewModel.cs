﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class UpdateTokenViewModel
    {
        public string UserPhone { get; set; }   
        public string Token { get; set; }   
    }
}
