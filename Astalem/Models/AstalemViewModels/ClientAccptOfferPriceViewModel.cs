﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astalem.Models.AstalemViewModels
{
    public class ClientAcceptOfferPriceViewModel    
    {
        public int OrderId { get; set; }
        public string ProviderPhone { get; set; }
    }
}
