﻿using Astalem.Data;
using Astalem.Models.AstalemViewModels;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace Hubs
{
  
        public class MainHub : Hub
        {

            private readonly ApplicationDbContext _context;
            public MainHub(ApplicationDbContext Context)
            {
                _context = Context;
            }



        public async Task SendLocation(TrackingViewModel trackingViewModel)     
        {
            double Lat = trackingViewModel.Lat;
            double Long = trackingViewModel.Log;
            string ProviderPhone = trackingViewModel.ProviderPhone;
            await Clients.Others.SendAsync("ReceiveLocation", Lat, Long, ProviderPhone);
        }


        public async Task SaveProviderConnectionId(string providerPhone)    
            {
            
            var provider = await _context.Providers.FindAsync(providerPhone);
            if (provider != null)
            {
                provider.ConnectionId = Context.ConnectionId;
                _context.Update(provider);
                await _context.SaveChangesAsync();
            }

        }

        }

    }
